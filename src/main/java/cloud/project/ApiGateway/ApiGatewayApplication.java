package cloud.project.ApiGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
//@EnableDiscoveryClient
public class ApiGatewayApplication {

       //@Autowired
       //private DiscoveryClient discoveryClient;

	public static void main(String[] args) {
		// Tell Boot to look for registration-server.yml
	    System.setProperty("spring.config.name", "registration-client");
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	
	/*@Bean
	RestTemplate restTemplate;
	
	@Autowired
	private DiscoveryClient discoveryClient;*/

}
