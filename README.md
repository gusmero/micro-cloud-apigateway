# Progetto APIGateway 

# Membri

- Francesco Faccioli (830190)
- Gusmara Andrea (831141)


# Introduzione al progetto Microservices

Questo progetto si pone l'obbiettivo di creare un'applicazione con caratteristiche cloud-native per la gestione d'informazione pubblicitaria di un'azienda di ristorazione e di possibili servizi erogabili dalla stessa. Il sistema viene presentato nel suo complesso in questo progetto [Microservices] (https://gitlab.com/gusmero/micro-cloud-hompage).



# Cloud-native application
La nostra applicazione presenta le caratteristiche cloud-native. 
Quindi utilizziamo il processo di sviluppo DevOps, specificando pipelines e stages che permettano di ottenere l'obbiettivo di un Continuous Integration e Continuous Deployment [CI/CD](#cicd). Inoltre il nostro sistema completo presenterà un'architettura a microservizi, elencati in questo paragrafo [Microservices](#Microservices), i quali vengono poi containerizzati tramite [Docker](https://www.docker.com/) e deployati con [Kubernetes](https://kubernetes.io/it/docs/concepts/overview/what-is-kubernetes/).
 




<a name="Microservices"></a> 
# Microservices Architecture

L'architettura del sistema presenta i seguenti microservizi:

- [Micro-cloud-HomePage](https://gitlab.com/gusmero/micro-cloud-hompage)
- [Micro-cloud-GestioneMenu](https://gitlab.com/gusmero/micro-cloud-gestionemenu)
- [Micro-cloud-nodejs](https://gitlab.com/ffaccioli/nodejsapi)
- [Micro-cloud-GestioneAccount](https://gitlab.com/gusmero/micro-cloud-gestioneaccount)
- [Micro-cloud-APIGateway](https://gitlab.com/gusmero/micro-cloud-apigateway)



<a name="apigateway"></a>
# Introduzione - APIGateway
Il progetto è una spring-app sviluppata con Spring Boot, che implementa il routing per il progetto Microservices.
Si basa sull'implementazione [Gateway](https://spring.io/projects/spring-cloud-gateway) di Spring Cloud.




# Specifiche
Vengono implementate due route, homepage e menu, che corrispondono logicamente ai corrispettivi servizi HomePage e GestioneMenu.
Abbiamo quindi un punto singolo di entrata delle richieste, mentre sarà il gateway a gestire l'indirizzamento verso l'apposito servizio.
Più in particolare, la scelta dell'indirizzamento si basa sulle richieste; quelle contententi homepage verranno reindirizzate a homepage ma con l'URL a cui viene tolto /homepage (predicato StripPrefix), quelle contenenti gestionemenu verranno reindirizzate a GestioneMenu, ma con l'URL al quale viene tolto /gestionemenu

```yaml
spring:
  application:
    name: apigateway
  cloud:
    gateway:
      routes:
      - id: homepage
        uri: http://homepage:4444
        predicates:
        - Path=/homepage/**
        filters:
        - StripPrefix=1
      - id: gestionemenu
        uri: http://menu:5556
        predicates:
        - Path=/gestionemenu/**
        filters:
        - StripPrefix=1
```

dopo che il servizio gateway viene tirato su in kubernetes , eseguiamo il seguente comando per esporre il servizio gateway


```sh
kubectl port-forward svc/gateway 8081:8081
```



<a name="DevOps"></a> 
# DevOps




<a name="cicd"></a> 
# CI/CD Pipelines
Di seguito si mostrano gli stage/job scelti per la nostra pipeline(CI/CD):

- **Build**
- **Package**
- **Release**
- **Deploy**





# Build

La build del progetto viene realizzata sfruttando l'apposito goal **compile** di Maven:

```maven
mvn clean compile -DskipTests
```




# Package

Nello stage di package tutte i file/librerie/dipendenze utilizzate dall'applicazione vengono compresse in un unico file con estensione .jar per facilitarne la distribuzione, in quanto un unico file "compresso" richiede un minore sforzo per essere trasferito sulla rete ed elaborato da un calcolatore.
La compressione non altera la struttura dei file stessi e inoltre slega l'applicazione dalla piattaforma nativa a patto di sfruttare una JVM per la sua esecuzione.
In maven questo viene realizzato attraverso l'apposito goal **package**:

```maven
mvn clean package
```
Viene quindi prodotto un unico file .jar all'interno della cartella target pronto per essere rilasciato e distribuito.

```maven
paths:
    - target/*.jar
```


<a name="Containerized"></a>
# Release

Durante lo stage di release ci affidiamo a Docker, in particolare a [Docker Engine](https://docs.docker.com/engine/) per la creazione automatica di un'immagine (micro-cloud-homepage) a partire dalle istruzioni contenute nel Dockerfile , l'immagine viene poi pushata nel repository wyst54rhrb pubblico.
All'inetrno del Dockerfile viene definito come ENTRYPOINT il comando 
```java
java -jar "ApiGateway-0.0.1-SNAPSHOT.jar"
```
per eseguire l'applicativo all'avvio del conteiner, tramite .jar che viene ottenuto nello stage di packag , e il comando EXPOSE per esporre la porta 2222 all'inetrno del container.
Grazie ai servizi di virtualizzazione a livello di sistema operativo offerti da Docker (Platform as a Service) siamo in grado di avere un container per la nostra applicazione che potrà essere runnato su qualsiasi server.

Connessione al DockerHub Registry:

```maven
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME
```



# Deploy

Infine, per quest'ultima fase sfruttiamo il servizio di cloud platforming (PaaS) [Azure](https://portal.azure.com/), in cui abbiamo installato il sistema open-source [Kubernetes](https://kubernetes.io/) per l'automazione del deployment , scaling e il management di un applicazione contenerizzata.
Con questi due strumenti otteniamo così una configurazione di continuos deploying che ci eravamo preposti con l'utilizzo del processo DevOps.

Lo stage di deploying viene organizzato con una prima connessione alla macchina remota tramite connessione ssh specificando il tipo di connessione PreferredAuthentications=publickey per favorire la connessione tramite chiave pubblica e private , e limitando la richiesta di password tramite l'option StrictHostKeyChecking=no .

Dopo aver effettuato la connessione alla macchina remota, ci spostiamo nela cartella specifica e pulliamo il progetto dal repository, nel quale è presente il file YAML di configurazione per il servizio e di deployment necessari. Grazie al nostro processo cicd e le configurazione delle policy di update ,  settante a rolling update , in ogni deployment object abbiamo così un aggiornamento da un sistema in esecuzione ad un'altro sistema in esecuzione. Permettendo una percentuale di disponibilità alta.

deploy:
  stage: deploy
  environment:
    name: staging
  before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    # Esegue ssh-agent
  - eval $(ssh-agent -s)
    # Aggiunta della chiave privata
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    # Creazione della cartella SSH e impostazione dei permessi
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
    # Viene disabilitato l'host key checking
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh -o StrictHostKeyChecking=no -o PreferredAuthentications=publickey -p 61414 studente@ml-lab-a08e3194-10fa-4b3b-b775-9e0c8a18d169.westeurope.cloudapp.azure.com "cd cloud-computing-lab/progetto/apigateway && git checkout main && git pull origin main && kubectl apply -f apigateway.yaml && exit"
  only:
    - main




