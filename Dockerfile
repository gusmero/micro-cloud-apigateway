########## Dockerfile #######################
##### utilziamo la jdk-11 come usata sulla nostra applicazione spring #######
FROM openjdk:11-jre

COPY  /target/ApiGateway-0.0.1-SNAPSHOT.jar ./


EXPOSE 8080
ENTRYPOINT ["java", "-jar", "ApiGateway-0.0.1-SNAPSHOT.jar"]

######################################################################

